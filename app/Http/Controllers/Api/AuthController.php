<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->except('photo'), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|',
            'birthday' => 'required|date'
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()], 422);
        }

        $name = '';
        if ($request->photo) {
            $colon_position = strpos($request->photo, ';');
            $string1 = substr($request->photo, 0, $colon_position);
            $string2 = explode(':', $string1)[1];
            $extension = explode('/', $string2)[1];
            $name = time() . '.' . $extension;

            Image::make($request->photo)->save(public_path('images/') . $name);
        }

        $request['password'] = Hash::make($request['password']);
        $request['birthday'] = Carbon::parse($request['birthday']);
        $user = User::create($request->except('photo') + ['filename' => $name]);

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token];

        return response($response, 200);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()], 422);
        }

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token];
                return response($response, 200);
            } else {
                $response = "Email and password does not match.";
                return response(['errors' => ['not_match' =>$response]], 404);
            }
        } else {
            $response = "Email and password does not match.";
            return response(['errors' => ['not_match' =>$response]], 404);
        }
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();

        $response = 'You have been succesfully logged out!';
        return response($response, 200);
    }
}
