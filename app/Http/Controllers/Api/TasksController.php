<?php

namespace App\Http\Controllers\Api;

use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TasksController extends Controller
{

    public function index()
    {
        $tasks = Task::where('user_id', request()->user()->id)
            ->orderBy('date', 'asc')
            ->orderBy('from', 'asc')
            ->get();

        $dates = $tasks->pluck('date')->unique();
        $dates = $dates->map(function ($date) {
            return Carbon::parse($date)->format('l, M d');
        });

        $tasks = $tasks->map(function ($task) {
            $task->date = Carbon::parse($task->date)->format('l, M d');
            $task->from = Carbon::parse($task->from)->format('g:i A');
            $task->to = Carbon::parse($task->to)->format('g:i A');
            return $task;
        });

        return response(['dates' => $dates, 'tasks' => $tasks]);
    }

    public function store(Request $request)
    {
        $user = $request->user();

        $attributes = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'date' => 'required',
            'from' => 'required',
            'to' => 'required',
            'location' => 'required',
            'color' => 'required'
        ]);

        $attributes['date'] = Carbon::parse($attributes['date']);
        $attributes['from'] = Carbon::parse($attributes['from']);
        $attributes['to'] = Carbon::parse($attributes['to']);

        $user->tasks()->create($attributes);
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return response(['Task deleted']);
    }
}
