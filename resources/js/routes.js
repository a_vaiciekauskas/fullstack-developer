import Register from "./components/Register";
import Login from "./components/Login";
import Home from "./components/Home";
import AddNew from "./components/AddNew";
import Menu from "./components/Menu";

const ifNotAuthenticated = (to, from, next) => {
    if (!localStorage.getItem('user-token')) {
        next();
        return;
    }
    next('/home');
}

const ifAuthenticated = (to, from, next) => {
    if (localStorage.getItem('user-token')) {
        next();
        return;
    }
    next('/');
}

export default {
    mode: 'history',
    routes: [
        {
            path: '/register',
            component: Register,
            name: 'register',
            beforeEnter: ifNotAuthenticated
        },
        {
            path: '/',
            component: Login,
            name: 'login',
            beforeEnter: ifNotAuthenticated
        },
        {
            path: '/home',
            component: Home,
            name: 'home',
            beforeEnter: ifAuthenticated
        },
        {
            path: '/add-new',
            component: AddNew,
            name: 'add-new',
            beforeEnter: ifAuthenticated
        },
        {
            path: '/menu',
            component: Menu,
            name: 'menu',
            beforeEnter: ifAuthenticated
        },
    ]
}
